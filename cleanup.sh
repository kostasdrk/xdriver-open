#!/bin/bash

# Cleanup script for dirty browser profiles and proxy intercomm files

rm -r ./browsers/config/chrome_profiles/*
rm -r ./browsers/config/opera_profiles/*
rm -r ./browsers/config/firefox_profiles/*-*

rm ./xutils/proxy/intercomm/*
